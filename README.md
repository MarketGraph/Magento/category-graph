# Category Graph

A data structure for Categories in Magento that scales to every product on earth!
- Has Local visibility settings!
- Navigation & breadcrumb based on Graph queries!
- Organize Stores!
- and Multi-vendor
- index into BlackCube Search